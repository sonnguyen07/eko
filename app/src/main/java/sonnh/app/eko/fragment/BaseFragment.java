package sonnh.app.eko.fragment;

import android.app.Fragment;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sonnh.app.eko.activity.MainActivity;

/**
 * Created by DELL on 7/10/2018.
 */

public abstract class BaseFragment extends Fragment {

    View contentView;
    ViewDataBinding binding;

    protected abstract int getScreenLayoutId();

    protected abstract void getBinding(ViewDataBinding binding);

    protected abstract void initView(View view);

    protected MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (contentView == null && getScreenLayoutId() != -1) {
            binding = DataBindingUtil.inflate(getActivity().getLayoutInflater(), getScreenLayoutId(), container, false);
            getMainActivity().setHeaderVisibility(View.VISIBLE);
            getBinding(binding);
            contentView = binding.getRoot();
            initView(contentView);
        }
        return contentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onDestroyView() {
        if (contentView.getParent() != null) ((ViewGroup) contentView.getParent()).removeView(contentView);
        super.onDestroyView();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
