package sonnh.app.eko.fragment.splash;


import android.databinding.ViewDataBinding;
import android.app.Fragment;
import android.os.Handler;
import android.view.View;

import sonnh.app.eko.App;
import sonnh.app.eko.R;
import sonnh.app.eko.fragment.BaseFragment;
import sonnh.app.eko.databinding.FragmentSplashBinding;
import sonnh.app.eko.fragment.login.LoginFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class SplashFragment extends BaseFragment {

    FragmentSplashBinding binding;

    @Override
    protected int getScreenLayoutId() {
        return R.layout.fragment_splash;
    }

    @Override
    protected void getBinding(ViewDataBinding binding) {
        this.binding = (FragmentSplashBinding) binding;
    }

    @Override
    protected void initView(View view) {
        getMainActivity().setHeaderVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getMainActivity().changeScreen(new LoginFragment(), App.SLIDE_FROM_RIGHT, false);
            }
        }, 5000);
    }

}
