package sonnh.app.eko.fragment.login;


import android.databinding.ViewDataBinding;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.simplepass.loading_button_lib.interfaces.OnAnimationEndListener;
import sonnh.app.eko.App;
import sonnh.app.eko.R;
import sonnh.app.eko.databinding.FragmentLoginBinding;
import sonnh.app.eko.fragment.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends BaseFragment {

    FragmentLoginBinding binding;

    @Override
    protected int getScreenLayoutId() {
        return R.layout.fragment_login;
    }

    @Override
    protected void getBinding(ViewDataBinding binding) {
        this.binding = (FragmentLoginBinding) binding;
    }

    @Override
    protected void initView(View view) {
        getMainActivity().setGradientColor(binding.loginContainer, Orientation.BL_TR, App.GRADIENT_COLOR);
        getMainActivity().setLeftRadius(binding.loginIconContainer);
//        binding.tvCopyright.setPaintFlags(binding.tvCopyright.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        binding.tvForgotPassword.setPaintFlags(binding.tvForgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        // Set login button animation when clicked
        binding.btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.btnlogin.startAnimation();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // You can change the button's background and icon when the loading is done
                        binding.btnlogin.doneLoadingAnimation(getResources().getColor(R.color.green), BitmapFactory.decodeResource(getResources(), R.drawable.ic_done_white_48dp));
                    }
                }, 2000);
            }
        });

    }

    public int dip2Px(float dpValue) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}
