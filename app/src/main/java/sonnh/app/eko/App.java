package sonnh.app.eko;

import android.app.Application;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;

import sonnh.app.eko.utils.FontUtil;

/**
 * Created by DELL on 7/10/2018.
 */

public class App extends Application{

    public final static int NO_ANIMATION = 0;
    public final static int SLIDE_FROM_RIGHT = 1;

    public final static String DEFAULT_FONT = "fonts/Nunito-SemiBold.ttf";
    public final static int SCREEN_MODE = WindowManager.LayoutParams.FLAG_FULLSCREEN;

    public static int[] GRADIENT_COLOR = {
            R.color.blue1,
            R.color.blue2,
    };

    public static final float DEFAULT_X_OFFSET = 0.0f;
    public static final float DEFAULT_Y_OFFSET = 0.0f;
    public static final float DEFAULT_SHADOW_ALPHA = 0.99f;
    public static final boolean DEFAULT_SHOW_WHEN_ALL_READY = true;
    public static final boolean DEFAULT_CALCULATE_ASYNC = true;
    public static final boolean DEFAULT_ANIMATE_SHADOW = true;
    public static final int DEFAULT_ANIMATION_TIME = 300;

    public static final int POS_UPDATE_ALL = -1;

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
