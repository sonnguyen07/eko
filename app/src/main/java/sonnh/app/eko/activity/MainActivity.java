package sonnh.app.eko.activity;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.view.View;

import sonnh.app.eko.App;
import sonnh.app.eko.R;
import sonnh.app.eko.fragment.splash.SplashFragment;
import sonnh.app.eko.databinding.ActivityMainBinding;
import sonnh.app.eko.utils.FontUtil;

public class MainActivity extends Activity {

    public ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appConfig();

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        initLayout();
        changeScreen(new SplashFragment(), App.NO_ANIMATION, false);
    }

    private void appConfig() {
        // Set up screen mode
        getWindow().setFlags(App.SCREEN_MODE, App.SCREEN_MODE);
        // Set up default font
        FontUtil.overrideFont(getApplicationContext(), "SERIF", App.DEFAULT_FONT);
    }

    private void initLayout(){
        GradientDrawable drawable = new GradientDrawable(Orientation.LEFT_RIGHT, new int[]{getResources().getColor(R.color.gray1), getResources().getColor(R.color.gray1)});
        drawable.setCornerRadius(10);
        binding.titleBar.setBackground(drawable);
    }

    public void changeScreen(Fragment fragment, int animation, boolean addBackStack) {
        FragmentManager manager = this.getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        if (addBackStack) transaction.addToBackStack(null);
        switch (animation) {
            case App.SLIDE_FROM_RIGHT:
                transaction.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_right_out);
                break;
            default:
                break;
        }

        transaction.replace(R.id.mainFrame, fragment);
        transaction.commit();
    }

    public void setGradientColor(View view, Orientation orientation, int[] listColor) {
        int[] colors = new int[listColor.length];
        for (int i = 0; i < listColor.length; i++) {
            colors[i] = getResources().getColor(listColor[i]);
        }
        GradientDrawable drawable = new GradientDrawable(orientation, colors);
        drawable.setCornerRadius(26);
        view.setBackground(drawable);
    }

    public void setLeftRadius(View view){
        PaintDrawable drawable = new PaintDrawable(getResources().getColor(R.color.white));
        drawable.setCornerRadii(new float[]{26, 26, 0, 0, 0, 0, 26, 26});
        view.setBackground(drawable);
    }

    public void setHeaderVisibility(int visibility){
        binding.mainHedear.setVisibility(visibility);
    }

}
