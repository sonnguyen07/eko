package sonnh.app.eko.interfaces;

/**
 * Created by DELL on 7/13/2018.
 */

public interface CustomizableByCode {
    void setSpinningBarWidth(float width);
    void setSpinningBarColor(int color);
    void setDoneColor(int color);
    void setPaddingProgress(float padding);
    void setInitialHeight(int height);
    void setInitialCornerRadius(float radius);
    void setFinalCornerRadius(float radius);
}
